<!--
Projeto: GEMEOS 2
Cliente: Ministério da Integração Nacional
Autor: Pacelly J. C. A. Xavier 
Data: 21 de Julho de 2017
Markdown com Instruções de Instalação do Ambiente de Backend
-->
# MI - GEMEOS
### Instalação do Ambiente de Frontend

---
#### Instalação das ferramentas

* [NodeJS](https://nodejs.org/en/download/)

#### Instalação das dependências
>Para instalar o Webpack em modo global:

>```bash
>$ npm i -g webpack
>```

>Para instalar as dependências do NodeJS execute:

>```bash
>$ npm i
>```

### Ações a serem seguidas após a instalação das dependências
>Deve-se utilizar os scripts descritos no *package.json*

> Iniciar a aplicação
>```bash
>$ npm start
>```

> Gerar arquivos para deploy
>```bash
>$ npm run build
>```

### Guia de codificação
#### Módulos
Os módulos são a "logica" da aplicação, refletindo suas funcionalidades.

A organização dos módulos é feita da seguinte forma:

+ src
* modules
