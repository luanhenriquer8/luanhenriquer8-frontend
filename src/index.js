import angular from 'angular';
import ngRoute from 'angular-route';
import ngAnimate from 'angular-animate';
import ngSanitize from 'angular-sanitize';
import ngMessages from 'angular-messages';
import uiRouter from 'angular-ui-router';
import uiRouterGrant from '../includes/lib/grant';
import oclazyload from 'oclazyload';
import restangular from 'restangular';
import ngStorage from 'ngstorage';
import uiBootstrap from 'angular-ui-bootstrap';
require('angular-sweetalert');
require ('angular-input-masks');
require('angular-moment-picker');
require('angular-block-ui');
require('angular-i18n/angular-locale_pt-br');

import {APP_NAME} from '../env';
import "./app.scss";

const app = angular.module(APP_NAME, [
    ngRoute,
    ngAnimate,
    ngSanitize,
    ngMessages,
    uiRouter,
    uiRouterGrant,
    oclazyload,
    restangular,
    // 'angular-block-ui',
    'oitozero.ngSweetAlert',
    uiBootstrap,
    ngStorage.name,
    'moment-picker',
    // 'blockUI',

]);

//app.service('OauthModel', OauthModel);

// import OauthInterceptorFactory from './services/factories/oauthInterceptor.factory';
// app.factory('OauthInterceptor', OauthInterceptorFactory);
// import OauthModel from './services/models/oauth.model';

import {
    setHtml5Mode,
    redirectToHome,
    restangular as restangularConfig,
    // blockConfig
    // injectToken
} from './config';

app.config(setHtml5Mode);
app.config(redirectToHome);
app.config(restangularConfig);
// app.config(blockConfig)
// app.config(injectToken);

import {setEnv} from '../includes/helpers/env';
app.constant('__env', setEnv());

import {grants} from './grant.rules';
app.run(grants);

import {bootstrapCss} from './../node_modules/bootstrap/dist/css/bootstrap.css';
app.run(bootstrapCss);

import {cssSweetAlert} from './../node_modules/angular-sweetalert/node_modules/sweetalert/lib/sweet-alert.css';
app.run(cssSweetAlert);

import {bootstrapJ} from './../node_modules/bootstrap/js/dist/util';
app.run(bootstrapJ);

import routes from './routes';
routes.forEach(route => app.config(route));

import {Bootstrap} from './bootstrap';

app.run(Bootstrap);

angular.bootstrap(document, [APP_NAME], {strictDi: true});