import angular from 'angular';

import LoginCtrl from './controllers/login.controller';
import {loginService} from './../../services/login.service'

const module = angular.module('login', [
    loginService.name
]);

module.controller('login.LoginCtrl', LoginCtrl);

export {module};