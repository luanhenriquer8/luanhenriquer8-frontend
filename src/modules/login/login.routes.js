import {LOGIN_TYPE, AUTH} from '../../../env';
import adminLayout from '../../layout/main.ng.html';
import loginView from './views/login.ng.html';

const loginRouter = ($stateProvider) => {
    const routes = [
        {
            name: 'login',
            url: '/login',
            abstract: true,
            templateUrl: adminLayout,
            resolve: {
                loadModule: ($q, $ocLazyLoad) => $q(resolve => {
                    require.ensure([], () => {
                        const module = require('./login.module').module;
                        $ocLazyLoad.load({name: module.name});
                        resolve(module.name);
                    });
                }),
                // onlyAuth: grant => grant.only({test: 'isAuthenticated', state: 'auth.login'})
            }
        },
        {
            name: 'login.main',
            url: '/',
            controller: 'login.LoginCtrl as vm',
            templateUrl: loginView,
            resolve: {
                // onlyAuth: grant => grant.only({test: 'isAuthenticated', state: 'auth.login'})
            }
        }
    ];

    routes.forEach(route => $stateProvider.state(route));
};

loginRouter.$inject = ['$stateProvider'];
export default loginRouter;