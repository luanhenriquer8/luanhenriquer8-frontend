function LoginCtrl($rootScope, LoginService) {

    self = this;
    self.email = null;
    self.password = null;
    self.loginForm = null;
    
    self.doLogin = function () {
        debugger;
        LoginService.doLogin(self.email, self.password).then(res => {
            console.log(res);
        }, error => {
            console.log(error);
        });

    }
}
LoginCtrl.$inject = ['$rootScope', 'LoginService'];

export default LoginCtrl;