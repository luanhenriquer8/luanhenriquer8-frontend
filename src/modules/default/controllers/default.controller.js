class DefaultCtrl {
    constructor($rootScope, DefaultService){

        let self = this;

        self.aTestVariableToShow = "Some test!";
        self.DS = DefaultService;
    }

    showVariable(){
        self.DS.aDefaultMethod().then(res => {
          console.log(res);
        }, error =>{})

    }

}

DefaultCtrl.$inject = ['$rootScope', 'DefaultService'];

export default DefaultCtrl;