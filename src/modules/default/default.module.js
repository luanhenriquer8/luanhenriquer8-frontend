/** Because of the import and inject of service modules i was not registering the controller  */


import {defaultService} from './../../services/default.service'

const module = angular.module('default', [
    defaultService.name
]);

import DefaultCtrl from './controllers/default.controller';


module.controller('default.DefaultCtrl', DefaultCtrl);

export {module};