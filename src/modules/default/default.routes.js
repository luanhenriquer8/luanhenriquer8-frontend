import {LOGIN_TYPE, AUTH} from '../../../env';
import adminLayout from '../../layout/main.ng.html';
import defaultView from './views/default.ng.html';

const defaultRouter = ($stateProvider) => {
    const routes = [
        {
            name: 'default',
            url: '',
            abstract: true,
            templateUrl: adminLayout,
            resolve: {
                loadModule: ($q, $ocLazyLoad) => $q(resolve => {
                    require.ensure([], () => {
                        const module = require('./default.module').module;
                        $ocLazyLoad.load({name: module.name});
                        resolve(module.name);
                    });
                }),
                // onlyAuth: grant => grant.only({test: 'isAuthenticated', state: 'auth.login'})
            }
        },
        {
            name: 'default.main',
            url: '/default',
            controller: 'default.DefaultCtrl as vm',
            templateUrl: defaultView,
            resolve: {
                // onlyAuth: grant => grant.only({test: 'isAuthenticated', state: 'auth.login'})
            }
        }
    ];

    routes.forEach(route => $stateProvider.state(route));
};

defaultRouter.$inject = ['$stateProvider'];
export default defaultRouter;