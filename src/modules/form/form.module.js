import angular from 'angular';

import SimpleFormCtrl from './controllers/form.simple.controller';
import TableCtrl from './controllers/form.table.controller';
import SimpleEditFormCtrl from './controllers/form.edit.simple.controller';
import CalendarCtrl from './controllers/form.calendar.controller';

import {formService} from './../../services/form.service';


const module = angular.module('simpleForm', [
    formService.name,

]);

module.controller('form.SimpleFormCtrl', SimpleFormCtrl);
module.controller('form.TableCtrl', TableCtrl);
module.controller('form.SimpleEditFormCtrl', SimpleEditFormCtrl);
module.controller('form.CalendarCtrl', CalendarCtrl);

export {module};