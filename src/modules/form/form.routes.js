import {LOGIN_TYPE, AUTH} from '../../../env';
import adminLayout from '../../layout/main.ng.html';
import simpleFormView from './views/form.simple.ng.html';
import tableFormView from './views/form.table.ng.html';
import tableEditFormView from './views/form.edit.simple.ng.html';
import calendarView from './views/form.calendar.ng.html';


/** Do not forget to import the router on the route.js on the base path*/
const formRouter = ($stateProvider) => {
    const routes = [
        {
            name: 'simpleForm',
            url: '/form',
            abstract: true,
            templateUrl: adminLayout,
            resolve: {
                loadModule: ($q, $ocLazyLoad) => $q(resolve => {
                    require.ensure([], () => {
                        const module = require('./form.module').module;
                        $ocLazyLoad.load({name: module.name});
                        resolve(module.name);
                    });
                }),
                // onlyAuth: grant => grant.only({test: 'isAuthenticated', state: 'auth.login'})
            }
        },
        {
            name: 'simpleForm.main',
            url: '/simple-form',
            controller: 'form.SimpleFormCtrl as vm',
            templateUrl: simpleFormView,
            resolve: {
                // onlyAuth: grant => grant.only({test: 'isAuthenticated', state: 'auth.login'})
            }
        },
        {
            name: 'simpleForm.tableForm',
            url: '/table-form',
            controller: 'form.TableCtrl as vm',
            templateUrl: tableFormView,
            resolve: {
                // onlyAuth: grant => grant.only({test: 'isAuthenticated', state: 'auth.login'})
            }
        },
        {
            name: 'simpleForm.mainEdit',
            url: '/simple-form-edit',
            controller: 'form.SimpleEditFormCtrl as vm',
            templateUrl: tableEditFormView,
            params: {
              personParam: null
            },
            resolve: {
                // onlyAuth: grant => grant.only({test: 'isAuthenticated', state: 'auth.login'})
            }
        },
        {
            name: 'simpleForm.calendar',
            url: '/calendar',
            controller: 'form.CalendarCtrl as vm',
            templateUrl: calendarView,
            params: {
              personParam: null
            },
            resolve: {
                // onlyAuth: grant => grant.only({test: 'isAuthenticated', state: 'auth.login'})
            }
        }
    ];

    routes.forEach(route => $stateProvider.state(route));
};

formRouter.$inject = ['$stateProvider'];
export default formRouter;