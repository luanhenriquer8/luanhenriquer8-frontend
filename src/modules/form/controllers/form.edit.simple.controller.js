function SimpleEditFormCtrl($rootScope, FormService, $state, $stateParams, SweetAlert) {

    self = this;

    let init = function () {
        self.personName = $stateParams.personParam.person.name;
        self.personEmail = $stateParams.personParam.person.email;
        self.personId = $stateParams.personParam.person.id;
        self.simpleForm = null;
    };

    self.updatePerson = function () {
        FormService.updatePerson(self.personId, self.personName, self.personEmail).then(res => {
            $state.go('simpleForm.tableForm');
        }, error => {
            SweetAlert.swal({
                    title: "Ii was not possible to edit that person!",
                    type: "warning",
                    confirmButtonColor: "#DC143C",
                    cancelButtonText: "No, cancel plx!",
                    closeOnConfirm: true,
                    closeOnCancel: false },
                );
        })
    };

    init();
}

SimpleEditFormCtrl.$inject = ['$rootScope', 'FormService', '$state', '$stateParams', 'SweetAlert'];

export default SimpleEditFormCtrl;