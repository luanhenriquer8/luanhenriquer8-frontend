function SimpleFormCtrl($rootScope, FormService, $state) {

    self = this;

    let init = function () {
        self.personName = null;
        self.personEmail = null;
        self.simpleForm = null;
    };

    self.createPerson = function () {
        FormService.createPerson(self.personName,self.personEmail).then(res => {
            $state.go('simpleForm.tableForm');
        }, error => {
            console.log("Error trying to create a new person! "+error);
        });
    };

    init();
}

SimpleFormCtrl.$inject = ['$rootScope', 'FormService', '$state'];

export default SimpleFormCtrl;