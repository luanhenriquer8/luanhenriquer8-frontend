function TableCtrl($rootScope, FormService, $state, SweetAlert){

    self = this;

    let init = function () {
        self.getPeople();
        self.person = {};
        self.personName = null;
        self.personEmail = null;
        self.simpleForm = null;
    };

    self.editPerson = function (person) {
        $state.go('simpleForm.mainEdit', {personParam: { "person" : person}});
    }

    self.getPeople = function () {
        // blockUI.start();
        FormService.getPeople().then(res => {
            self.people = res.plain();
            // blockUI.stop();
        }, error => {
          console.log("Error trying to show!");
        });
    };

    self.deletePerson = function(person){
        SweetAlert.swal({
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DC143C",confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: false },
            function(isConfirm){
                if (isConfirm) {
                    FormService.deletePerson(person.id).then(res => {
                            self.people.splice(self.people.indexOf(person), 1);
                            SweetAlert.swal("Deleted!", "Deleted Successfull", "success");
                        }, error => {
                            SweetAlert.swal("Error", "Error trying to delete person", "error");
                        });
                } else {
                    SweetAlert.swal("Cancelled", "" +
                        "Delete Canceled Successfull", "error");
                }
            });
    };

    init();
}

TableCtrl.$inject = ['$rootScope', 'FormService', '$state', 'SweetAlert'];

export default TableCtrl;