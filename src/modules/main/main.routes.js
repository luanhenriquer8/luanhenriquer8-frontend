import {LOGIN_TYPE, AUTH} from '../../../env';
import adminLayout from '../../layout/main.ng.html';
import mainView from './views/main.ng.html';

const mainRouter = ($stateProvider) => {
    const routes = [
        {
            name: 'main',
            url: '',
            abstract: true,
            templateUrl: adminLayout,
            resolve: {
                loadModule: ($q, $ocLazyLoad) => $q(resolve => {
                    require.ensure([], () => {
                        const module = require('./main.module').module;
                        $ocLazyLoad.load({name: module.name});
                        resolve(module.name);
                    });
                }),
                // onlyAuth: grant => grant.only({test: 'isAuthenticated', state: 'auth.login'})
            }
        },
        {
            name: 'main.main',
            url: '/',
            controller: 'main.MainCtrl as vm',
            templateUrl: mainView,
            resolve: {
                // onlyAuth: grant => grant.only({test: 'isAuthenticated', state: 'auth.login'})
            }
        }
    ];

    routes.forEach(route => $stateProvider.state(route));
};

mainRouter.$inject = ['$stateProvider'];
export default mainRouter;