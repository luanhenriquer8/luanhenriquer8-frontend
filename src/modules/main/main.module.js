import angular from 'angular';

import MainCtrl from './controllers/main.controller';

const module = angular.module('main', [
]);

module.controller('main.MainCtrl', MainCtrl);

export {module};