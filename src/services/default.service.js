import {APP_NAME} from '../../env';

class DefaultService{
    constructor(env, Restangular){
        Object.assign(this, Restangular.service('/'));
    }

    aDefaultMethod(){
        // let req = this.one('').get();
        // console.log(req);
        // return req;
    }
}

DefaultService.$inject = ['__env','Restangular'];

//Export fora dos modulos do angular
//import UtilModel from 'path/to/model'
//Export para os modulos do angular
//import {utilModel} from 'path/to/model'
const defaultService =  angular.module(APP_NAME).service('DefaultService', DefaultService);
export default DefaultService;

export {defaultService};
