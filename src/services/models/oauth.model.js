import {APP_NAME,AUTH_CADU,CLIENT_SCOPE, GRANT_TYPE, CLIENT_ID, CLIENT_SECRET} from '../../../env';

class OauthModel {
    constructor(env, Restangular, session) {
        this.session = session;
        this.service = Restangular.withConfig(function (restangular) {
            restangular.setBaseUrl(AUTH_CADU);
        }).service('');
        Object.assign(this,this.service);
    }

    authenticate(username, password) {
        let vm = this;
        const token = vm.one('oauth').one('token').customPOST({}, "", {
            "grant_type": GRANT_TYPE,
            "client_id": CLIENT_ID,
            "client_secret": CLIENT_SECRET,
            'scope': CLIENT_SCOPE,
            "username": username,
            "password": password
        }, {});
        token.then(function (res) {
            vm.session.token = res.token_type+' '+res.access_token;
            vm.session.scope = res.scope;
            vm.session.name = username ;
           return  vm.one('api').one('private').one('permissoes').customGET('',{},{'authorization': vm.session.token });


        }, function (error) {
            //todo: tratar o erro
        }).then(function (success) {
            vm.session.authorities =  success.authorities
        },function (error) {

        })

        return token;
    }

    logout() {
        delete this.session.token;
    }
}

OauthModel.$inject = ['__env', 'Restangular', '$sessionStorage'];

export default OauthModel;