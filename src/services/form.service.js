import angular from "angular";
import {APP_NAME} from "../../env";

class FormService{
    constructor(env, Restangular){
        Object.assign(this, Restangular.service('person/'));
    }

    getPeople(){
        let req = this.one('').get();
        return req;
    }

    deletePerson(id){
        let req = this.one(id).remove();
        return req;
    }

    createPerson(name, email){
        let req = this.one('').post('', {name, email});
        return req;
    }

    updatePerson(id, name, email){
        let req = this.one().customPUT({id, name, email}, null, null, null);
        return req;
    }
}

FormService.$inject = ['__env','Restangular'];

export default FormService;
const formService =  angular.module(APP_NAME).service('FormService', FormService);
export {formService};
