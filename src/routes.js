import defaultRouter from './modules/default/default.routes';
import mainRouter from './modules/main/main.routes';
import formRouter from './modules/form/form.routes';


export default [
    defaultRouter,
    mainRouter,
    formRouter
];