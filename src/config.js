import {APP_NAME, GRANT_TYPE, CLIENT_ID, CLIENT_SECRET,API_BASE_PATH} from './../env';


const setHtml5Mode = $locationProvider => $locationProvider.html5Mode(true).hashPrefix('*');
setHtml5Mode.$inject = ['$locationProvider'];
export {setHtml5Mode}

const redirectToHome = $urlRouterProvider => {
    $urlRouterProvider.otherwise('/');
};

redirectToHome.$inject = ['$urlRouterProvider'];
export {redirectToHome}

const restangular = (RestangularProvider, env, $qProvider, $sceProvider) => {
    RestangularProvider.setBaseUrl(env.API_BASE_PATH);
    RestangularProvider.setDefaultHeaders({'Access-Control-Allow-Origin' : 'http://localhost:8085'});

    // RestangularProvider.setDefaultRequestParams(['get', 'post', 'put', 'delete', 'patch']
    //     , {
    //     'XDEBUG_SESSION_START':'PHPSTORM',
    // }
    // );
    // $qProvider.errorOnUnhandledRejections(false);
    // console.log($sceProvider)
};

restangular.$inject = ['RestangularProvider', '__env', '$qProvider'];
export {restangular};

// const blockConfig = (blockUIConfig) => {
//     blockUIConfig.message = 'Loading...';
//     blockUIConfig.resetOnException = false;
// };
//
// blockConfig.$inject = ['blockUIConfig'];
// export {blockConfig};

// const injectToken = ($httpProvider) => $httpProvider.interceptors.push(() => {
//     return {
//         'request': config => {
//             const token = sessionStorage.getItem('token');
//             config.headers.Authorization = sessionStorage.getItem('token') && 'Bearer ' + JSON.parse(sessionStorage.getItem('token')).access_token;
//             return config;
//         }
//     }
// });
//
// injectToken.$inject = ['$httpProvider'];
// export {injectToken};